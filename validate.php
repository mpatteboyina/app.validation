<?php
// 1
define('SUCCESS', '0');
// 2
function result($status, $error = '') { return array(
'status' => $status, 'error' => $error
);
}
// 3
function logToFile($msg) {
$fd = fopen('log.txt', 'a');
$str = '[' . date('Y/m/d h:i:s', mktime()) . '] ' . $msg; fwrite($fd, $str . "\r\n");
fclose($fd);
}

// 4
function validateReceipt($receipt, $sandbox) {
logToFile(print_r(array($receipt, $sandbox), true));
return result(SUCCESS); }
// 5
$receipt = $_POST['receipt'];
$sandbox = $_POST['sandbox'];
$retval = validateReceipt($receipt, $sandbox);
// 6
header('content-type: application/json; charset=utf-8'); echo json_encode($retval);
?>